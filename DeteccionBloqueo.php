<?php

class DeteccionBloqueo {
    
    private $recursosDisponibles;
    private $recursosEnExistencia;
    private $asignacionActual;
    private $solicitudes;

    public function __construct($recursosEnExistencia, $recursosDisponibles, $asignacionActual, $solicitudes)
    {
        $this->recursosDisponibles = $recursosDisponibles;
        $this->recursosEnExistencia = $recursosEnExistencia;
        $this->asignacionActual = $asignacionActual;
        $this->solicitudes = $solicitudes;
    }
    
    public function detectarBloqueo() 
    {
        $cantidadProcesos = count($this->asignacionActual);
        $cantidadDeRecursos = count($this->recursosDisponibles);
        $p = array_fill(0, $cantidadProcesos, 0);
        

        for ($k=0; $k<$cantidadProcesos; $k++) {
            for ($i=0; $i<$cantidadProcesos; $i++) {
                if ($p[$i] === 0) {
                    $r = array_fill(0, $cantidadDeRecursos, 0);
                    for ($j=0; $j<$cantidadDeRecursos; $j++) {
                        if ($this->solicitudes[$i][$j] <= $this->recursosDisponibles[$j]) {
                            $r[$j] = 1;
                        } else {
                            break;
                        }
                    }
                }
                if (array_sum($r) == $cantidadDeRecursos) { // si los recursos estan disponibles
                    $p[$i] = 1; // marca el proceso como finalizado
                    for ($j=0; $j<$cantidadDeRecursos;$j++) { // libera los recursos y actualiza la matriz de asignacion y el vecto de reursos disponibles
                        $this->recursosDisponibles[$j] = $this->asignacionActual[$i][$j] + $this->recursosDisponibles[$j];
                        $this->asignacionActual[$i][$j] = 0;
                    }
                }
            }
        }
    }


    public function mostrarEstado()
    {
        echo "Recursos en existencia " . implode(" ", $this->recursosEnExistencia) . "\n";
        echo "Recursos disponibles " . implode(" ", $this->recursosDisponibles) . "\n";   
        echo "Matriz de asignación actual\n";
        $this->mostrarMatriz($this->asignacionActual);
        echo "Matriz de solicitudes\n";
        $this->mostrarMatriz($this->solicitudes);
    }

    private function mostrarMatriz($matriz)
    {
        foreach ($matriz as $fila) {
            echo implode(" ", $fila) . "\n";
        }
        echo "\n";
    }
}