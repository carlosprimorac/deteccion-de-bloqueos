# Detección del interbloqueo con varios recursos de cada tipo

Se presenta una implementación de un algoritmo basado en matrices para detectar bloqueos entre _n_ procesos de _P<sub>1</sub>_ a _P<sub>n</sub>_
El número de clases de recursos es _m_, con _E<sub>1</sub> recursos de la clase 1, _E<sub>2</sub>_ recursos de la clase 2 y en general _E<sub>i</sub> recursos de la clase i (1 <= _i_ <= _m_). _E_ el **vector de recursos existentes**.
El **vector de recursos disponibles es _A_, donde _A<sub>i_</sub> proporciona el número de instancias del recurso _i_ que está disponible en un momento dado.
Con _C_ y _R_ se designan las matrices de **asignación actual** y **solicitudes** respectivamente. La _i_ésima fila de _C_ indica cuántas instancias de cada clase de recurso contiene _P<sub>i</sub>_ en un momento dado. 
Así _C<sub>ij</sub>_ es el número de instancias del recurso _j_ que están contenidas por el proceso _i_. De manera similar, _R<sub>ij</sub>_ es el número de instancias del recurso _j_ que desea _P<sub>i</sub>_.


